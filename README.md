# 简介
不定期更新优化。

| 软件       | 版本    | 功能           | 地址                             |
|----------|-------|--------------|--------------------------------|
| Anaconda | 5.3   | Python集成开发环境 | https://www.anaconda.com/      |
| Django   | 3.1.7 | Web框架        | https://www.djangoproject.com/ |
| Scrapy   | 2.4.1 | 爬虫框架         | https://scrapy.org/            |
| Echarts   | 5.0.2 | Echarts图表         | https://eacharts.apache.org/            |

基于Python和Echarts职位画像系统，使用Scrapy抓取职位招聘数据，使用Django+echarts完成数据可视化。

# 环境安装配置
建议直接安装anconda，然后git clone后进入项目目录，执行pip install -r requirements.txt


# 项目说明
## 1. 数据抓取（DataSpider）
基于Scrapy爬虫框架，职位数据抓取模块

## 2. 数据清洗（DataClean）
设置定时器，对数据进行清洗

## 3. 数据可视化（DataView）
### 基于Django的职位画像系统

### shell文件夹为定时执行脚本，主要作用如下：
1. 数据库数据进行数据统计分析并缓存
2. 根据统计分析数据生成echarts页面

# 欢迎Issues